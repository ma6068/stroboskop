# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://ma6068@bitbucket.org/ma6068/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/ma6068/stroboskop/commits/d6c75e5fe471694347ebd2bf69f36bb7a03ceb0b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ma6068/stroboskop/commits/9cfce36469224d670ee59fe457227b8b53da64fc

Naloga 6.3.2:
https://bitbucket.org/ma6068/stroboskop/commits/233dfadc456819f2149044e94fe8461ab9d4723e

Naloga 6.3.3:
https://bitbucket.org/ma6068/stroboskop/commits/aa921296290c18293288d8d855d3898e19885b9c

Naloga 6.3.4:
https://bitbucket.org/ma6068/stroboskop/commits/62079389604eb19bbdaf0226c92482ffad7f46c3

Naloga 6.3.5:

```
git checkout origin master 
git merge master izgled 
git push origin master 
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ma6068/stroboskop/commits/157e30f3a6d6c3ee8d1e8c148e5abe8e04e7cecb

Naloga 6.4.2:
https://bitbucket.org/ma6068/stroboskop/commits/0b1775a8ed018c6d58bc5f41d694b25d9273cec5

Naloga 6.4.3:
https://bitbucket.org/ma6068/stroboskop/commits/5806919190f6f38a55e1e893cfec46a2204ace89

Naloga 6.4.4:
https://bitbucket.org/ma6068/stroboskop/commits/5c501da521a0944dfc2defb42577881659acd870